package com.example.API_Webshop.infrastructure;

import com.example.API_Webshop.domaine.Customer;
import com.example.API_Webshop.domaine.NotFoundException;
import com.example.API_Webshop.domaine.ServerException;
import com.example.API_Webshop.infrastructure.api.mapping.CustomerApiDtoMapper;
import com.example.API_Webshop.infrastructure.api.serialization.CustomerApiDto;
import com.example.API_Webshop.infrastructure.api.serialization.ProductApiDto;
import com.example.API_Webshop.services.CustomerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(consumes = {"*/*"}, produces = {"application/json"})
public class CustomerRestController {
    private final static String customersResourceName = "/customers";
    private final CustomerService customerService;
    private final CustomerApiDtoMapper customerApiDtoMapper;

    @GetMapping(CustomerRestController.customersResourceName + "/{id}")
    public CustomerApiDto getCustomer(@PathVariable int id) throws ServerException, NotFoundException {
        Customer customer = customerService.getCustomerById(id);
        return this.customerApiDtoMapper.toCustomerApiDto(customer);
    }
    public CustomerRestController(CustomerService customerService, CustomerApiDtoMapper customerApiDtoMapper) {
        this.customerService = customerService;
        this.customerApiDtoMapper = customerApiDtoMapper;
    }
}