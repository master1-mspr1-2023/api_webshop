package com.example.API_Webshop.infrastructure;

import com.example.API_Webshop.services.CustomerService;
import com.example.API_Webshop.services.CustomerStorageInterface;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerServiceConfiguration {
    @Bean
    public CustomerService getCustomerService(CustomerStorageInterface customerStorageInterface) {
        return new CustomerService(customerStorageInterface);
    }
}
