package com.example.API_Webshop.infrastructure.exceptions;

import com.example.API_Webshop.domaine.NotFoundException;
import com.example.API_Webshop.domaine.ServerException;
import com.example.API_Webshop.infrastructure.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
@RequestMapping(produces = "text/plain")
public class GlobalExceptionHandler {
    @org.springframework.web.bind.annotation.ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public final ResponseEntity<ApiError> notFoundExceptionHandler(NotFoundException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.NOT_FOUND,e.getMessage()),HttpStatus.NOT_FOUND);
    }
    @org.springframework.web.bind.annotation.ExceptionHandler({ServerException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final ResponseEntity<ApiError> serverExceptionHandler(ServerException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
