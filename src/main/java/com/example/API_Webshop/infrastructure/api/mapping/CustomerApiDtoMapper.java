package com.example.API_Webshop.infrastructure.api.mapping;

import com.example.API_Webshop.domaine.Customer;
import com.example.API_Webshop.infrastructure.api.serialization.CustomerApiDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", uses = {InstantMapper.class,PositiveNumberMapper.class}, injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface CustomerApiDtoMapper {
    CustomerApiDto toCustomerApiDto(Customer customer);
    Customer toCustomer (CustomerApiDto customerApiDto);
}
