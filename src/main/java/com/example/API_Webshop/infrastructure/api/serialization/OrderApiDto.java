package com.example.API_Webshop.infrastructure.api.serialization;

import com.example.API_Webshop.domaine.Customer;
import com.example.API_Webshop.domaine.Product;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;

public record OrderApiDto(int id, OffsetDateTime createdAt, List<ProductApiDto> productList) {
}
