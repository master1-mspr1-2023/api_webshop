package com.example.API_Webshop.infrastructure.api.serialization;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

public record ProductApiDto(int id, OffsetDateTime createdAt, String name, BigDecimal price, String description, String color, int stock) {
}
