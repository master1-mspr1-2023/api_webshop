package com.example.API_Webshop.infrastructure.api.serialization;

import java.util.List;

public record CustomerApiDto(int id, String familyName, String firstName, List<OrderApiDto> orderList) {
}