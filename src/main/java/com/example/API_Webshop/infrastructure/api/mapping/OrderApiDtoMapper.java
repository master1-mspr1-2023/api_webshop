package com.example.API_Webshop.infrastructure.api.mapping;

import com.example.API_Webshop.domaine.Order;
import com.example.API_Webshop.domaine.Order;
import com.example.API_Webshop.infrastructure.api.serialization.OrderApiDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.ERROR,
        uses = {InstantMapper.class, PositiveNumberMapper.class })
public interface OrderApiDtoMapper {
    Order toOrder(OrderApiDto OrderApiDto);
    List<Order> toOrderList(List<OrderApiDto> OrderApiDtoList);

    List<OrderApiDto> toOrderDtoList(List<Order> Orders);

    OrderApiDto toOrderDto(Order Order);
}
