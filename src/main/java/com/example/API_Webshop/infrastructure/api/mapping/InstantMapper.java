package com.example.API_Webshop.infrastructure.api.mapping;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Component
public class InstantMapper {
    public Instant map(OffsetDateTime value) {
        return value.toInstant();
    }
    public OffsetDateTime map(Instant value) {
        return value.atOffset(ZoneOffset.UTC);
    }
}
