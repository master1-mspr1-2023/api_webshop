package com.example.API_Webshop.infrastructure.api.mapping;

import com.example.API_Webshop.domaine.Product;
import com.example.API_Webshop.infrastructure.api.serialization.ProductApiDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.ERROR,
        uses = { PositiveNumberMapper.class, InstantMapper.class })
public interface ProductApiDtoMapper {
    Product toProduct(ProductApiDto productApiDto);
    List<Product> toProductList(List<ProductApiDto> productApiDtoList);

    List<ProductApiDto> toProductDtoList(List<Product> products);

    ProductApiDto toProductDto(Product product);
}
