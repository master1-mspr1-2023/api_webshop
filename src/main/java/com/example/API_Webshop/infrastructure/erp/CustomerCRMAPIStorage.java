package com.example.API_Webshop.infrastructure.erp;

import com.example.API_Webshop.domaine.*;
import com.example.API_Webshop.services.CustomerStorageInterface;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerCRMAPIStorage implements CustomerStorageInterface {
    private final ApiProperties properties;
    private final RestTemplate restTemplate;

    public CustomerCRMAPIStorage(ApiProperties properties, RestTemplate restTemplate) {
        this.properties = properties;
        this.restTemplate = restTemplate;
    }

    public Customer getCustomerById(int id) {
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler(){
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return response.getStatusCode().isError();
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                if (response.getStatusCode().is5xxServerError()) {
                    throw new NotFoundException("L'id demandé n'a pas été trouvé");
                }
            }
        });
        List<Order> orders = new ArrayList<>();
        CustomerDTO customerWithAssociatedOrders = restTemplate.exchange(properties.getCustomersUrl() + "/" + id, HttpMethod.GET, null, CustomerDTO.class).getBody();
        List<OrderDTO> orderDTOList = customerWithAssociatedOrders.orders();
        for (OrderDTO orderDTO : orderDTOList) {
            List<Product> orderDomainProducts = new ArrayList<>();
            List<ProductDTO> productDTOList = this.getOrderDtoProductList(orderDTO,customerWithAssociatedOrders);
            for (ProductDTO productDTO : productDTOList) {
                Product product = new Product(Integer.parseInt(productDTO.id()), productDTO.createdAt().toInstant(), productDTO.name(), new PositiveNumber<BigDecimal>(new BigDecimal(productDTO.details().price())), productDTO.details().description(), productDTO.details().color(), new PositiveNumber<Integer>(productDTO.stock()));
                orderDomainProducts.add(product);
            }
            orders.add(new Order(Integer.parseInt(orderDTO.id()),orderDTO.createdAt().toInstant(),orderDomainProducts));
        }
        return new Customer(customerWithAssociatedOrders.id(), customerWithAssociatedOrders.lastName(), customerWithAssociatedOrders.firstName(), orders);
    }
    public List<ProductDTO> getOrderDtoProductList(OrderDTO orderDTO, CustomerDTO customerDTO) {
        String url = properties.getCustomersUrl() + "/" + customerDTO.id() + "/orders/" + orderDTO.id() + "/products";
        return restTemplate.exchange(url,HttpMethod.GET,null,new ParameterizedTypeReference<List<ProductDTO>>() {}).getBody();
    }
}
