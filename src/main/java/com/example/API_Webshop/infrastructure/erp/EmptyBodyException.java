package com.example.API_Webshop.infrastructure.erp;

import org.springframework.web.client.RestClientException;

public class EmptyBodyException extends RestClientException {
    public EmptyBodyException() {
        super("Response body cannot be null");
    }
}
