package com.example.API_Webshop.infrastructure.erp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ErpConfiguration {
    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
