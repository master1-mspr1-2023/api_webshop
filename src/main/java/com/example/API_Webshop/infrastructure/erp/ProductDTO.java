package com.example.API_Webshop.infrastructure.erp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.OffsetDateTime;
@JsonIgnoreProperties(ignoreUnknown = true)
public record ProductDTO(OffsetDateTime createdAt, String name, ProductDetailsDTO details, int stock, String id) {

}
