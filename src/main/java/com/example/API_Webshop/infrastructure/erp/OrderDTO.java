package com.example.API_Webshop.infrastructure.erp;

import java.time.OffsetDateTime;

public record OrderDTO(OffsetDateTime createdAt,String id, String customerId) {
}
