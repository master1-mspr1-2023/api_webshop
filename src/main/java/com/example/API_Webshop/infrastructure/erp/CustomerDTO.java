package com.example.API_Webshop.infrastructure.erp;

import java.util.List;


public record CustomerDTO(int id, String lastName, String firstName, List<OrderDTO> orders) {
    }
