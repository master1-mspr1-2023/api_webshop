package com.example.API_Webshop.infrastructure.erp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public record ProductDetailsDTO(String price, String description, String color) {
}
