package com.example.API_Webshop.infrastructure.erp;

import com.example.API_Webshop.domaine.NotFoundException;
import com.example.API_Webshop.domaine.PositiveNumber;
import com.example.API_Webshop.domaine.Product;
import com.example.API_Webshop.domaine.ServerException;
import com.example.API_Webshop.services.ProductStorageInterface;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.rmi.ServerError;
import java.util.List;
import java.util.Optional;
@Component
public class ProductERPAPIProductStorage implements ProductStorageInterface {
    private ApiProperties properties;
    private final RestTemplate restTemplate;
    public List<Product> getProducts() {
        HttpEntity<List<ProductDTO>> response = restTemplate.exchange(
                properties.getProductsUrl(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ProductDTO>>() {
                }
        );
        List<ProductDTO> productsDTOs = Optional.ofNullable(response.getBody()).orElseThrow(EmptyBodyException::new);
        List<Product> products = productsDTOs.stream().map((productDTO) -> {
            Product product = new Product(Integer.parseInt(productDTO.id()), productDTO.createdAt().toInstant(), productDTO.name(), new PositiveNumber<BigDecimal>(new BigDecimal(productDTO.details().price())), productDTO.details().description(), productDTO.details().color(), new PositiveNumber<Integer>(productDTO.stock()));
            return product;
        }).toList();
        return products;
    }

    @Override
    public Product getProduct(int id) throws NotFoundException,ServerException {
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler(){
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return response.getStatusCode().isError();
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                if (response.getStatusCode().is5xxServerError()) {
                    throw new NotFoundException("L'id demandé n'a pas été trouvé");
                }
            }
        });
        var response = restTemplate.exchange(
                properties.getProductsUrl() + "/" + id,
                HttpMethod.GET,
                null,
                String.class
        );
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        try {
            ProductDTO productDTO = objectMapper.readValue(response.getBody(),ProductDTO.class);
            return new Product(Integer.parseInt(productDTO.id()), productDTO.createdAt().toInstant(), productDTO.name(), new PositiveNumber<BigDecimal>(new BigDecimal(productDTO.details().price())), productDTO.details().description(), productDTO.details().color(), new PositiveNumber<Integer>(productDTO.stock()));
        }
        catch (JsonProcessingException | IllegalArgumentException e) {
            throw new ServerException("Une erreur s'est produite lors du traitement du JSON", e);
        }
    }

    public ProductERPAPIProductStorage(ApiProperties properties, RestTemplate restTemplate) {
        this.properties = properties;
        this.restTemplate = restTemplate;
    }
}
