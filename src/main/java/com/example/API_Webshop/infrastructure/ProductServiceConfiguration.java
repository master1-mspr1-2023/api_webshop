package com.example.API_Webshop.infrastructure;

import com.example.API_Webshop.services.ProductService;
import com.example.API_Webshop.services.ProductStorageInterface;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductServiceConfiguration {
    @Bean
    public ProductService productService(ProductStorageInterface productStorageInterface) {
        return new ProductService(productStorageInterface);
    }
}
