package com.example.API_Webshop.infrastructure;

import com.example.API_Webshop.domaine.NotFoundException;
import com.example.API_Webshop.domaine.Product;
import com.example.API_Webshop.domaine.ServerException;
import com.example.API_Webshop.infrastructure.api.mapping.ProductApiDtoMapper;
import com.example.API_Webshop.infrastructure.api.serialization.ProductApiDto;
import com.example.API_Webshop.services.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(consumes = {"*/*"}, produces = {"application/json"})
public class ProductRestController {
    private final static String productsResourceName = "/products";
    private final ProductService productService;
    private final ProductApiDtoMapper productApiDtoMapper;

    @GetMapping(ProductRestController.productsResourceName)
    public List<ProductApiDto> getProducts() throws URISyntaxException {
        List<Product> products = productService.getProducts();
        return productApiDtoMapper.toProductDtoList(products);
    }
    @GetMapping(ProductRestController.productsResourceName + "/{id}")
    public ProductApiDto getProduct(@PathVariable int id) throws ServerException, NotFoundException {
        Product product = productService.getProduct(id);
        return productApiDtoMapper.toProductDto(product);
    }
    public ProductRestController(ProductService productService, ProductApiDtoMapper productApiDtoMapper) {
        this.productService = productService;
        this.productApiDtoMapper = productApiDtoMapper;
    }
}
