package com.example.API_Webshop.services;

import com.example.API_Webshop.domaine.NotFoundException;
import com.example.API_Webshop.domaine.Product;
import com.example.API_Webshop.domaine.ServerException;

import java.net.URISyntaxException;
import java.util.List;

public interface ProductStorageInterface {
    List<Product> getProducts();

    Product getProduct(int id) throws NotFoundException, ServerException;
}
