package com.example.API_Webshop.services;

import com.example.API_Webshop.domaine.Customer;

public class CustomerService {
    private CustomerStorageInterface customerStorageInterface;

    public CustomerService(CustomerStorageInterface customerStorageInterface) {
        this.customerStorageInterface = customerStorageInterface;
    }

    public Customer getCustomerById(int id) {
        return this.customerStorageInterface.getCustomerById(id);
    }
}
