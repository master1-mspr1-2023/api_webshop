package com.example.API_Webshop.services;

import com.example.API_Webshop.domaine.Customer;

public interface CustomerStorageInterface {
    Customer getCustomerById(int id);
}
