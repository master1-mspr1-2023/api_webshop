package com.example.API_Webshop.services;

import com.example.API_Webshop.domaine.NotFoundException;
import com.example.API_Webshop.domaine.Product;
import com.example.API_Webshop.domaine.ServerException;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

public class ProductService {
    private final ProductStorageInterface productStorageInterface;
    public ProductService(ProductStorageInterface productStorageInterface) {
        this.productStorageInterface = Objects.requireNonNull(productStorageInterface);
    }
    public List<Product> getProducts() throws URISyntaxException {
        return this.productStorageInterface.getProducts();
    }

    public Product getProduct(int id) throws ServerException, NotFoundException {
        return this.productStorageInterface.getProduct(id);
    }
}
