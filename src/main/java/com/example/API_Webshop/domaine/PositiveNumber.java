package com.example.API_Webshop.domaine;

import java.util.Objects;

public class PositiveNumber<T extends Number> {
    private T number;
    public PositiveNumber(T number) {
        this.number = Objects.requireNonNull(number);
        if (number.intValue() < 0) {
            throw new IllegalArgumentException();
        }
    }

    public T getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof PositiveNumber<?>)) {
            return false;
        }
        else {
            PositiveNumber objectToCompare = (PositiveNumber<?>) obj;
            return this.toString().equals(objectToCompare.toString());
        }
    }

    @Override
    public String toString() {
        return this.number.toString();
    }
}
