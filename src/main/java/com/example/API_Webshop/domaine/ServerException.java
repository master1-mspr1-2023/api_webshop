package com.example.API_Webshop.domaine;

public class ServerException extends Exception{
    public ServerException(String message, Throwable cause) {
        super(message, cause);
    }
}
