package com.example.API_Webshop.domaine;

import java.time.Instant;
import java.util.List;
import java.util.Objects;

public class Order {
    private int id;
    private Instant createdAt;
    private List<Product> productList;

    public Order(int id, Instant createdAt, List<Product> productList) {
        this.id = id;
        this.createdAt = Objects.requireNonNull(createdAt);
        this.productList = Objects.requireNonNull(productList);
    }

    public int getId() {
        return this.id;
    }

    public List<Product> getProductList() {
        return this.productList;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }
}
