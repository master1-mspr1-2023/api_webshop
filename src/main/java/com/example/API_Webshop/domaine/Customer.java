package com.example.API_Webshop.domaine;

import java.util.List;
import java.util.Objects;

public class Customer {
    private int id;
    private String familyName;
    private String firstName;
    private List<Order> orderList;

    public Customer(int id, String familyName, String firstName, List<Order> orderList) {
        this.id = id;
        this.familyName = Objects.requireNonNull(familyName);
        this.firstName = Objects.requireNonNull(firstName);
        this.orderList = Objects.requireNonNull(orderList);
        if (this.familyName.isBlank() || this.firstName.isBlank()) {
            throw new IllegalArgumentException();
        }
    }

    public int getId() {
        return this.id;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public List<Order> getOrderList() {
        return this.orderList;
    }
}
