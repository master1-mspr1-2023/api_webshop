package com.example.API_Webshop.domaine;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public class Product {
    private int id;
    private Instant createdAt;
    private String name;
    private PositiveNumber<BigDecimal> price;
    private String description;
    private String color;
    private PositiveNumber<Integer> stock;

    public PositiveNumber<Integer> getStock() {
        return stock;
    }


    public Product(int id, Instant createdAt, String name, PositiveNumber<BigDecimal> price, String description, String color, PositiveNumber<Integer> stock) {
        this.id = id;
        this.createdAt = Objects.requireNonNull(createdAt);
        this.name = Objects.requireNonNull(name);
        this.price = Objects.requireNonNull(price);
        this.description = Objects.requireNonNull(description);
        this.color = Objects.requireNonNull(color);
        this.stock = Objects.requireNonNull(stock);
        if (this.name.isBlank() || this.color.isBlank()) {
            throw new IllegalArgumentException();
        }
    }

    public int getId() {
        return id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public String getName() {
        return name;
    }

    public PositiveNumber<BigDecimal> getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", createdAt=" + createdAt +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", color='" + color + '\'' +
                ", stock=" + stock +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Product)) {
            return false;
        } else {
            Product product = (Product) obj;
            boolean identicalContent = this.id == product.id && this.createdAt.equals(product.createdAt) && this.name.equals(product.name) && this.price.equals(product.price) && this.description.equals(product.description) && this.color.equals(product.color);
            return identicalContent;
        }
    }
}
