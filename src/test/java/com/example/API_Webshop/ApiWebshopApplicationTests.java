package com.example.API_Webshop;

import com.example.API_Webshop.domaine.Product;
import com.example.API_Webshop.infrastructure.erp.ProductERPAPIProductStorage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URISyntaxException;
import java.util.List;

@SpringBootTest
class ApiWebshopApplicationTests {

	@Test
	void contextLoads() {
	}
/*
	@Test
	@DisplayName("QUAND J'APPELLE L'API des produits, alors une liste de produits est retournée")
	void getProductsReturnsProductDTOList() throws URISyntaxException {
		ProductERPAPIProductStorage productErpapiStorage = new ProductERPAPIProductStorage();
		List<Product> products = productErpapiStorage.getProducts();
	}
*/
}
