package com.example.API_Webshop.domaine;

import java.util.List;

public class CustomerBuilder {
    private int id;
    private String familyName;
    private String firstName;
    private List<Order> orderList;


    public CustomerBuilder() {
        this.id = 5;
        this.familyName = "Doe";
        this.firstName = "John";
    }

    public CustomerBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public CustomerBuilder setFamilyName(String familyName) {
        this.familyName = familyName;
        return this;
    }

    public CustomerBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }
    public Customer build() {
        return new Customer(this.id,this.familyName,this.firstName,this.orderList);
    }

    public CustomerBuilder setOrderList(List<Order> orderList) {
        this.orderList = orderList;
        return this;
    }
}
