package com.example.API_Webshop.domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.function.BiFunction;
import java.util.function.BooleanSupplier;
import java.util.stream.Stream;

public class ProductTest {
    private static Stream<Arguments> productBuilderSetters() {
        return Stream.of(
                Arguments.of((BiFunction<ProductBuilder, String, ProductBuilder>) ProductBuilder::setName),
                Arguments.of((BiFunction<ProductBuilder, String, ProductBuilder>) ProductBuilder::setDescription),
                Arguments.of((BiFunction<ProductBuilder, String, ProductBuilder>) ProductBuilder::setColor),
                Arguments.of((BiFunction<ProductBuilder, PositiveNumber<BigDecimal>, ProductBuilder>) ProductBuilder::setPrice),
                Arguments.of((BiFunction<ProductBuilder, PositiveNumber<Integer>, ProductBuilder>) ProductBuilder::setStock)
        );

    }

    private static Stream<Arguments> getterTestFunctionProvider() {
        return Stream.of(
                Arguments.of(
                        new BooleanSupplier() {
                            @Override
                            public boolean getAsBoolean() {
                                Product product = new ProductBuilder().setId(5).build();
                                return product.getId() == 5;
                            }
                        }

                ),
                Arguments.of(
                        new BooleanSupplier() {
                            @Override
                            public boolean getAsBoolean() {
                                Instant createdAtInstant = Instant.now();
                                Product product = new ProductBuilder().setCreatedAt(createdAtInstant).build();
                                return product.getCreatedAt() == createdAtInstant;
                            }
                        }

                ),
                Arguments.of(
                        new BooleanSupplier() {
                            @Override
                            public boolean getAsBoolean() {
                                String name = "Ferrari F1";
                                Product product = new ProductBuilder().setName(name).build();
                                return product.getName() == name;
                            }
                        }
                ),
                Arguments.of(
                        new BooleanSupplier() {
                            @Override
                            public boolean getAsBoolean() {
                                PositiveNumber<BigDecimal> price = new PositiveNumber<>(BigDecimal.valueOf(5.0));
                                Product product = new ProductBuilder().setPrice(price).build();
                                return product.getPrice() == price;
                            }
                        }

                ),
                Arguments.of(
                        new BooleanSupplier() {
                            @Override
                            public boolean getAsBoolean() {
                                String description = "A really fast car";
                                Product product = new ProductBuilder().setDescription(description).build();
                                return product.getDescription() == description;
                            }
                        }
                ),
                Arguments.of(
                        new BooleanSupplier() {
                            @Override
                            public boolean getAsBoolean() {
                                String color = "Red";
                                Product product = new ProductBuilder().setColor(color).build();
                                return product.getColor() == color;
                            }
                        }
                ),
                Arguments.of(
                        new BooleanSupplier() {
                            @Override
                            public boolean getAsBoolean() {
                                PositiveNumber<Integer> stock = new PositiveNumber<>(1);
                                Product product = new ProductBuilder().setStock(stock).build();
                                return product.getStock() == stock;
                            }
                        }
                ));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "    "})
    @DisplayName("QUAND je crée un produit avec un nom vide ou constitué de caractères blancs, alors une exception est lancée")
    public void creatingAProductWithEmptyOrWhitespacesComposedNameThrowsException(String value) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Product product = new ProductBuilder().setName(value).build();
        });
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "    "})
    @DisplayName("QUAND je crée un produit avec une couleur vide ou constituée de caractères blancs, alors une exception est lancée")
    public void creatingAProductWithEmptyOrWhitespacesComposedColorThrowsException(String value) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Product product = new ProductBuilder().setColor(value).build();
        });
    }

    @ParameterizedTest
    @MethodSource("productBuilderSetters")
    @DisplayName("QUAND je crée un produit avec un attribut égal à null, alors une exception est lancée")
    public <T> void creatingACustomerWithNullFamilyNameThrowsException(BiFunction<ProductBuilder, T, ProductBuilder> setter) {
        Assertions.assertThrows(NullPointerException.class, () -> {
            setter.apply(new ProductBuilder(), null).build();
        });
    }

    @ParameterizedTest
    @MethodSource("getterTestFunctionProvider")
    @DisplayName("Quand j'appelle un getter sur un objet pour récupérer la valeur d'un attribut, alors la valeur de l'attribut est récupérée")
    public void getterTest(BooleanSupplier f) {
        Assertions.assertTrue(f.getAsBoolean());
    }
    @Test
    @DisplayName("ÉTANT DONNÉ deux Produits, QUAND ils sont identiques, ALORS la méthode equals retourne true")
    public void equalsReturnsTrueForIdenticalProducts() {
        Instant productOneInstant = Instant.now();
        Instant productTwoInstant = Instant.from(productOneInstant);
        Product productOne = new Product(5,productOneInstant,"Ferrari",new PositiveNumber<BigDecimal>(BigDecimal.valueOf(350000)),"A Ferrari","Red",new PositiveNumber<Integer>(3));
        Product productTwo = new Product(5,productTwoInstant,"Ferrari",new PositiveNumber<BigDecimal>(BigDecimal.valueOf(350000)),"A Ferrari","Red",new PositiveNumber<Integer>(3));
        Assertions.assertTrue(productOne.equals(productTwo));
    }
}
