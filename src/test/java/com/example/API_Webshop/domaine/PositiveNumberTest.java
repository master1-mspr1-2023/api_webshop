package com.example.API_Webshop.domaine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

public class PositiveNumberTest {
    @ParameterizedTest
    @ValueSource(ints = {0,1})
    @DisplayName("Quand je crée un objet nombre Positif avec une valeur <x> positive ou nulle, ALORS le nombre créé a cette valeur CAS 0 1")
    public void createAPositiveOrZeroValuedPositiveNumlberWorks(int x) {
        PositiveNumber<Integer> positiveNumber = new PositiveNumber<>(x);
        Assertions.assertEquals(new PositiveNumber<Integer>(x),positiveNumber);
    }
    @ParameterizedTest
    @NullSource
    @DisplayName("Quand je crée un objet nombre Positif avec une valeur <x> égale à null, ALORS une exception est lancée")
    public void createAPositiveOrZeroValuedPositiveNumberWorks(Integer x) {
        Assertions.assertThrows(NullPointerException.class, () -> {
            PositiveNumber<Integer> positiveNumber = new PositiveNumber<>(x);
        });
    }
    @Test
    @DisplayName("Étant donné deux objets Nombre Positifs ayant la même valeur, QUAND on les compare, vec la méthode equals, alors la méthode retourne true")
    public void theEqualsMethodReturnsTrueForEqualNumbers() {
        PositiveNumber<Integer> positiveNumberOne = new PositiveNumber<>(5);
        PositiveNumber<Integer> positiveNumberTwo = new PositiveNumber<>(5);
        Assertions.assertTrue(positiveNumberOne.equals(positiveNumberTwo));
    }
    @Test
    @DisplayName("Étant donné deux objets Nombre Positifs ayant une valeur différente, QUAND on les compare, vec la méthode equals, alors la méthode retourne true")
    public void theEqualsMethodReturnsFalseForUnequalNumbers() {
        PositiveNumber<Integer> positiveNumberOne = new PositiveNumber<>(5);
        PositiveNumber<Integer> positiveNumberTwo = new PositiveNumber<>(3);
        Assertions.assertFalse(positiveNumberOne.equals(positiveNumberTwo));
    }
    @Test
    @DisplayName("La méthode getNumber retourne la valeur stockée dans le nombre Positif")
    public void getNumberReturnsThePositiveNumbersValue() {
        PositiveNumber<Integer> integerPositiveNumber = new PositiveNumber<Integer>(5);
        Assertions.assertEquals(Integer.valueOf(5), integerPositiveNumber.getNumber());
    }
}
