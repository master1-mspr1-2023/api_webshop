package com.example.API_Webshop.domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class OrderBuilderTest {
    @Test
    @DisplayName("QUAND je crée un builder de commande en l'associant à une liste de produits ALORS le builder de commande est associé à cette liste de produits")
    public void orderBuilderCreatedWithProductListHasProductList() {
        List<Product> productList = new ArrayList<Product>();
        productList.add(new Product(5, Instant.now(),"Ferrari",new PositiveNumber<BigDecimal>(BigDecimal.valueOf(350000)),"A Ferrari","Red",new PositiveNumber<Integer>(3)));
        OrderBuilder orderBuilder = new OrderBuilder().setProductList(productList);
        Assertions.assertEquals(productList,orderBuilder.getProductList());
    }
}
