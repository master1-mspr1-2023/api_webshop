package com.example.API_Webshop.domaine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class OrderTest {
    @Test
    @DisplayName("QUAND on crée une commande en mettant un id en paramètre du builder , alors la valeur de l'attribut id de la commande est la vzleur de l'id passé en paramètre")
    public void orderBuilderParameterizedWithDefinedIdHasId() {
        int id = 5;
        Order order = new OrderBuilder().setId(id).setProductList(new ArrayList<>()).setCreatedAt(Instant.now()).build();;
        Assertions.assertEquals(id, order.getId());
    }
    @Test
    @DisplayName("QUAND je crée une commande en l'associant à une liste de produits ALORS la commande est associée à cette liste de produits")
    public void orderCreatedWithProductListHasProductList() {
        List<Product> productList = new ArrayList<Product>();
        productList.add(new Product(5, Instant.now(),"Ferrari",new PositiveNumber<BigDecimal>(BigDecimal.valueOf(350000)),"A Ferrari","Red",new PositiveNumber<Integer>(3)));
        Order order = new OrderBuilder().setProductList(productList).setCreatedAt(Instant.now()).build();;
        Assertions.assertEquals(productList,order.getProductList());
    }
    @ParameterizedTest
    @NullSource
    @DisplayName("QUAND je crée une commande avec une liste de produits égal à null, ALORS une exception est lancée")
    public void creatingOrderWithNullProductListAttributeThrowsException(List<Product> productList) {
        Assertions.assertThrows(NullPointerException.class, ()-> {
            int id = 5;
            Order order = new OrderBuilder().setId(id).setProductList(productList).setCreatedAt(Instant.now()).build();;
        } );
    }
    @ParameterizedTest
    @NullSource
    @DisplayName("Quand je crée une commande avec un Instant de création égal à null, alors une exception est lancée")
    public void creatingOrderWithCreationInstantEqualToNullThrowsException(Instant createdAt) {
        Assertions.assertThrows(NullPointerException.class, () -> {
            int id = 5;
            List<Product> productList = new ArrayList<Product>();
            new OrderBuilder().setId(id).setProductList(productList).setCreatedAt(createdAt).build();
        });
    }
}
