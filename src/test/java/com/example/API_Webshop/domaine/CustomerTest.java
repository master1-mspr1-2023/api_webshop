package com.example.API_Webshop.domaine;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.stream.Stream;

public class CustomerTest {
    private static Stream<Arguments> getterTestFunctionProvider() {
        return Stream.of(
                Arguments.of(
                        new BooleanSupplier() {
                            @Override
                            public boolean getAsBoolean() {
                                int idInitialValue = 5;
                                Customer customer = new CustomerBuilder().setId(idInitialValue).setOrderList(new ArrayList<Order>()).build();
                                return customer.getId() == idInitialValue;
                            }
                        }

                ),
                Arguments.of(
                        new BooleanSupplier() {
                            @Override
                            public boolean getAsBoolean() {
                                String familyName = "Stork";
                                Customer customer = new CustomerBuilder().setFamilyName(familyName).setOrderList(new ArrayList<Order>()).build();
                                return customer.getFamilyName() == familyName;
                            }
                        }

                ),
                Arguments.of(
                        new BooleanSupplier() {
                            @Override
                            public boolean getAsBoolean() {
                                String firstName = "Ferrari F1";
                                Customer customer = new CustomerBuilder().setFirstName(firstName).setOrderList(new ArrayList<Order>()).build();
                                return customer.getFirstName() == firstName;
                            }
                        }
                )
        );
    }

    @Test
    @DisplayName("QUAND je crée un client avec un ID, ALORS le client a cet ID")
    public void customerCreatedWithIdParameterHasId() {
        Customer customer = new Customer(5, "Doe", "John",new ArrayList<Order>() );
        Assertions.assertEquals(5, customer.getId());
    }

    @Test
    @DisplayName("QUAND je crée un client avec un nom de famille, alors le client a ce nom de famille")
    public void customerCreatedWithFamilyNameHasFamilyName() {
        String familyName = "Doe";
        Customer customer = new Customer(5, familyName, "John", new ArrayList<Order>());
        Assertions.assertEquals(familyName, customer.getFamilyName());
    }

    @Test
    @DisplayName("QUAND je crée un client avec un prénom, alors le client a ce prénom")
    public void customerCreatedWithFirstNameHasFirstName() {
        String firstName = "John";
        Customer customer = new Customer(5, "Doe", firstName, new ArrayList<Order>());
        Assertions.assertEquals(firstName, customer.getFirstName());
    }

    @Test
    @DisplayName("QUAND je crée un client avec un nom de famille égal à null, alors une exception est lancée")
    public void creatingACustomerWithNullFamilyNameThrowsException() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            Customer customer = new Customer(5, null, "John", new ArrayList<Order>());
        });
    }

    @Test
    @DisplayName("QUAND je crée un client avec un prénom égal à null, alors une exception est lancée")
    public void creatingACustomerWithNullFirstNameThrowsException() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            Customer customer = new Customer(5, "Doe", null, new ArrayList<Order>());
        });
    }

    @Test
    @DisplayName("QUAND je crée un client avec un nom de famille vide, alors une exception est lancée")
    public void creatingACustomerWithEmptyFamilyNameThrowsException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Customer customer = new Customer(5, "", "John", new ArrayList<Order>());
        });
    }

    @Test
    @DisplayName("QUAND je crée un client avec un prénom vide, alors une exception est lancée")
    public void creatingACustomerWithEmptyFirstNameThrowsException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Customer customer = new Customer(5, "Doe", "", new ArrayList<Order>());
        });
    }

    @Test
    @DisplayName("QUAND je crée un client avec un nom de famille contenant seulement des caractères blancs, alors une exception est lancée")
    public void creatingACustomerWithFamilyNameComposedOnlyOfWhitespacesThrowsException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Customer customer = new Customer(5, "    ", "John", new ArrayList<Order>());
        });
    }

    @Test
    @DisplayName("QUAND je crée un client avec un prénom contenant seulement des caractères blancs, alors une exception est lancée")
    public void creatingACustomerWithFirstNameComposedOnlyOfWhitespacesThrowsException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Customer customer = new Customer(5, "Doe", "  ", new ArrayList<Order>());
        });
    }

    @ParameterizedTest
    @NullSource
    @DisplayName("QUAND je crée un client avec une liste de commandes égal à null, ALORS une exception est lancée")
    public void creatingOrderWithNullOrderListAttributeThrowsException(List<Order> orderList) {
        Assertions.assertThrows(NullPointerException.class, () -> {
            int id = 5;
            String familyName = "Doe";
            String firstName = "John";
            Customer customer = new CustomerBuilder().setId(id).setFamilyName(familyName).setFirstName(firstName).setOrderList(orderList).build();
        });
    }
    @Test
    @DisplayName("QUAND je crée un client avec une liste de commandes, alors le client a cette lieste de commandes")
    public void customerCreatedWithOrderListHasOrderList() {
        List<Order> orderList = new ArrayList<>();
        Customer customer = new CustomerBuilder().setId(5).setFamilyName("Doe").setFirstName("John").setOrderList(orderList).build();
        Assertions.assertEquals(orderList,customer.getOrderList());
    }
    @ParameterizedTest
    @MethodSource("getterTestFunctionProvider")
    @DisplayName("QUAND j'appelle un getter sur un objet pour récupérer la valeur d'un attribut, ALORS la valeur de l'attribut est récupérée")
    public void getterTest(BooleanSupplier f) {
        Assertions.assertTrue(f.getAsBoolean());
    }
}