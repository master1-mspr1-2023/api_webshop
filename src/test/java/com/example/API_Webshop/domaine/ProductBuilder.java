package com.example.API_Webshop.domaine;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public class ProductBuilder {
    private int id;
    private Instant createdAt;
    private String name;
    private PositiveNumber<BigDecimal> price;
    private String description;
    private String color;
    private PositiveNumber<Integer> stock;


    public ProductBuilder() {
        this.id = 5;
        this.createdAt = Instant.now();
        this.name = "Toto";
        this.price = new PositiveNumber<BigDecimal>(BigDecimal.valueOf(50));
        this.description = "Toto";
        this.color = "yellow";
        this.stock = new PositiveNumber<Integer>(50);
    }

    public ProductBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public ProductBuilder setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ProductBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ProductBuilder setPrice(PositiveNumber<BigDecimal> price) {
        this.price = price;
        return this;
    }

    public ProductBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public ProductBuilder setColor(String color) {
        this.color = color;
        return this;
    }

    public ProductBuilder setStock(PositiveNumber<Integer> stock) {
        this.stock = stock;
        return this;
    }
    public Product build() {
        return new Product(this.id,this.createdAt,this.name,this.price,this.description,this.color,this.stock);
    }
}
