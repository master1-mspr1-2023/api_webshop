package com.example.API_Webshop.domaine;

import java.time.Instant;
import java.util.List;

public class OrderBuilder {
    private int id;
    private List<Product> productList;
    private Instant createdAt;

    public OrderBuilder setId(int id) {
        this.id = id;
        return this;
    }


    public Order build() {
        return new Order(this.id, this.createdAt, this.productList);
    }

    public OrderBuilder setProductList(List<Product> productList) {
        this.productList = productList;
        return this;
    }

    public List<Product> getProductList() {
        return this.productList;
    }

    public OrderBuilder setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }
}
