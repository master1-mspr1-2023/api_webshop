package com.example.API_Webshop.infrastructure.api.mapping;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public class InstantMapperTest {
    @Test
    public void mappingFromOffsetDateTimeToInstantWorks() {
        Instant instant = Instant.now();
        OffsetDateTime offsetDateTime = instant.atOffset(ZoneOffset.UTC);
        InstantMapper instantMapper = new InstantMapper();
        Assertions.assertEquals(instantMapper.map(offsetDateTime), instant);
    }
    @Test
    public void mappingFromInstantToOffsetDateTimeWorks() {
        Instant instant = Instant.now();
        OffsetDateTime offsetDateTime = instant.atOffset(ZoneOffset.UTC);
        InstantMapper instantMapper = new InstantMapper();
        Assertions.assertEquals(offsetDateTime, instantMapper.map(instant));
    }
}
