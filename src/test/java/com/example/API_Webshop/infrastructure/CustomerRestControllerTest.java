package com.example.API_Webshop.infrastructure;

import com.example.API_Webshop.domaine.Customer;
import com.example.API_Webshop.domaine.Order;
import com.example.API_Webshop.domaine.PositiveNumber;
import com.example.API_Webshop.domaine.Product;
import com.example.API_Webshop.infrastructure.api.mapping.CustomerApiDtoMapper;
import com.example.API_Webshop.infrastructure.api.security.SecurityConfig;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.oauth2Login;
import com.example.API_Webshop.infrastructure.api.serialization.CustomerApiDto;
import com.example.API_Webshop.infrastructure.erp.CustomerDTO;
import com.example.API_Webshop.infrastructure.erp.OrderDTO;
import com.example.API_Webshop.infrastructure.erp.ProductDTO;
import com.example.API_Webshop.infrastructure.erp.ProductDetailsDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@SpringBootTest
@ContextConfiguration
@AutoConfigureMockMvc
@ActiveProfiles("test")

public class CustomerRestControllerTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CustomerApiDtoMapper customerApiDtoMapper;
    @MockBean
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }
    @Test
    public void sendingRequestToProductsEndpointWithoutTokenIsUnauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/customers/3").accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }
    @Test
    @DisplayName("QUAND j'appelle l'API pour récupérer la liste des commandes d'un client avec les produits associés, alors j'obtiens une liste de commandes avec les produits associés")
    public void callingTheRestControllerToRetreiveOrderListRetrievesOrderList() throws Exception {
        var fakeErpOrders = new ArrayList<OrderDTO>();
        var productsOfEachFakeErpOrder = new ArrayList<ProductDTO>();
        var intermediateDomainOrders = new ArrayList<Order>();
        var intermediateDomainProducts = new ArrayList<Product>();


        for (int i = 0; i < 5; i++) {
            ProductDTO productDTO = sampleProductDto(i);
            OrderDTO orderDTO = sampleOrderDto(i);
            productsOfEachFakeErpOrder.add(productDTO);
            fakeErpOrders.add(orderDTO);

            Product intermediateDomainProduct = new Product(Integer.parseInt(productDTO.id()), productDTO.createdAt().toInstant(), productDTO.name(), new PositiveNumber<BigDecimal>(new BigDecimal(productDTO.details().price())), productDTO.details().description(), productDTO.details().color(), new PositiveNumber<Integer>(productDTO.stock()));
            intermediateDomainProducts.add(intermediateDomainProduct);
            Order intermediateDomainOrder = new Order(Integer.parseInt(orderDTO.id()),orderDTO.createdAt().toInstant(),intermediateDomainProducts);
            intermediateDomainOrders.add(intermediateDomainOrder);
        }
        //Création du client retourné par le mock de l'API DTO avec la liste de produits
        CustomerDTO mockReturnedCustomer = new CustomerDTO(1,"Doe","John",fakeErpOrders);
        Customer customer = new Customer(mockReturnedCustomer.id(),mockReturnedCustomer.lastName(),mockReturnedCustomer.firstName(),intermediateDomainOrders);
        CustomerApiDto expectedCustomer = customerApiDtoMapper.toCustomerApiDto(customer);

        //Paramétrage de Mockito pour mocker le retour de l'API qui récupère les infos du client avec les commandes associées
        Mockito.when(restTemplate.exchange(
                Mockito.eq("http://localhost:8888/customers/1"),
                Mockito.eq(HttpMethod.GET),
                Mockito.isNull(),
                Mockito.eq(CustomerDTO.class)
        )).thenReturn(ResponseEntity.ok(mockReturnedCustomer));
        //Paramétrage de Mockito pour la partie 2) Appel d’API pour récupérer la liste des produits de la commande de ce client (Construction d’une liste d’objets OrderDTO)
        Mockito.when(restTemplate.exchange(
                Mockito.matches("^http:\\/\\/localhost:8888\\/customers\\/1\\/orders\\/\\d+\\/products$"),
                Mockito.eq(HttpMethod.GET),
                Mockito.isNull(),
                Mockito.any(ParameterizedTypeReference.class)
        )).thenReturn(ResponseEntity.ok(productsOfEachFakeErpOrder));
        var response = mockMvc.perform(MockMvcRequestBuilders.get("/customers/1").accept(MediaType.APPLICATION_JSON).with(oauth2Login().authorities(new SimpleGrantedAuthority("SCOPE_message:read")))).andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse();
        var body = response.getContentAsByteArray();
        var returnedCustomer = objectMapper.readValue(body, CustomerApiDto.class);
        Assertions.assertThat(expectedCustomer).isEqualTo(returnedCustomer);
    }

    private static OrderDTO sampleOrderDto(int i) {
        return new OrderDTO(OffsetDateTime.now(ZoneId.of("UTC")), "1", "Commande " + i);
    }

    private static ProductDTO sampleProductDto(int i) {
        return new ProductDTO(OffsetDateTime.now(ZoneId.of("Europe/Paris")), "Toto" + i, new ProductDetailsDTO(Integer.toString(i), "Article" + i, "Couleur" + i), i, Integer.toString(i));
    }
}
