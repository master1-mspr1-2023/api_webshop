package com.example.API_Webshop.infrastructure;

import com.example.API_Webshop.domaine.PositiveNumber;
import com.example.API_Webshop.domaine.Product;
import com.example.API_Webshop.infrastructure.api.mapping.ProductApiDtoMapper;
import com.example.API_Webshop.infrastructure.api.serialization.ProductApiDto;
import com.example.API_Webshop.infrastructure.erp.ApiProperties;
import com.example.API_Webshop.infrastructure.erp.ProductDTO;
import com.example.API_Webshop.infrastructure.erp.ProductDetailsDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.oauth2Login;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@SpringBootTest
@ContextConfiguration
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ProductRestControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;
    @MockBean
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductApiDtoMapper apiDtoMapper;
    @Autowired
    private ApiProperties apiProperties;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }
    @Test
    void getProductsReturnsAllProducts() throws Exception {
        List<ProductDTO> mockReturnedList = new ArrayList<>();
        List<Product> expectedProductList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ProductDTO productDTO = new ProductDTO(OffsetDateTime.now(ZoneId.of("Europe/Paris")),"Toto" + i, new ProductDetailsDTO(Integer.toString(i),"Article" + i,"Couleur" + i), i, Integer.toString(i));
            Product product = new Product(Integer.parseInt(productDTO.id()), productDTO.createdAt().toInstant(), productDTO.name(), new PositiveNumber<BigDecimal>(new BigDecimal(productDTO.details().price())), productDTO.details().description(), productDTO.details().color(), new PositiveNumber<Integer>(productDTO.stock()));
            mockReturnedList.add(productDTO);
            expectedProductList.add(product);
        }

        Mockito.when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(HttpMethod.GET),
                Mockito.isNull(),
                Mockito.any(ParameterizedTypeReference.class)
        )).thenReturn(ResponseEntity.ok(mockReturnedList));
        var response = mockMvc.perform(MockMvcRequestBuilders.get("/products").accept(MediaType.APPLICATION_JSON).with(oauth2Login().authorities(new SimpleGrantedAuthority("SCOPE_message:read")))).andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse();
        var body = response.getContentAsByteArray();
        var apiDtoProducts = objectMapper.readValue(body, new TypeReference<List<ProductApiDto>>() {});
        var products = apiDtoMapper.toProductList(apiDtoProducts);
        Assertions.assertThat(products).containsExactlyInAnyOrderElementsOf(expectedProductList);
    }
    @Test
    void getProductsWithIdReturnsProductsWithGivenId() throws Exception {
        String jsonString = "{\"createdAt\":\"2022-07-10T21:46:07.114Z\",\"name\":\"Ms. Jo Nolan\",\"details\":{\"price\":\"375.00\",\"description\":\"The Football Is Good For Training And Recreational Purposes\",\"color\":\"violet\"},\"stock\":15979,\"id\":\"3\"}";
        ProductDTO productDTO = objectMapper.readValue(jsonString,ProductDTO.class);
        Product expectedProduct = new Product(Integer.parseInt(productDTO.id()), productDTO.createdAt().toInstant(), productDTO.name(), new PositiveNumber<BigDecimal>(new BigDecimal(productDTO.details().price())), productDTO.details().description(), productDTO.details().color(), new PositiveNumber<Integer>(productDTO.stock()));
        Mockito.when(restTemplate.exchange(
                Mockito.eq(apiProperties.getProductsUrl() + "/3"),
                Mockito.eq(HttpMethod.GET),
                Mockito.isNull(),
                Mockito.eq(String.class)
        )).thenReturn(ResponseEntity.ok(jsonString));
        var response = mockMvc.perform(MockMvcRequestBuilders.get("/products/3").accept(MediaType.APPLICATION_JSON).with(oauth2Login().authorities(new SimpleGrantedAuthority("SCOPE_message:read")))).andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse();
        var body = response.getContentAsByteArray();
        var apiDtoProduct = objectMapper.readValue(body, ProductApiDto.class);
        var product = apiDtoMapper.toProduct(apiDtoProduct);
        Assertions.assertThat(product).isEqualTo(expectedProduct);
    }
    @ParameterizedTest
    @ValueSource(strings = {"/products","/products/3"})
    public void sendingRequestToProductsEndpointWithoutTokenIsUnauthorized(String endpoint) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(endpoint).accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }
}
